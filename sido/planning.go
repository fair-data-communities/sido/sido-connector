package sido

import(
  "encoding/xml"
  "fmt"
  "strings"
  "strconv"

  "purplepolarbear.nl/wfs_service/gml"
)

//
// API
//

type Planning struct {
  XMLName   xml.Name
  Id        string `xml:"gml:id,attr"`

  LocalName string `xml:"SidO:LocalName"`
  ObjectId  string `xml:"SidO:OBJECTID"`
  Provider  string `xml:"SidO:Provider"`
  Status    string `xml:"SidO:Status"`
  Accuracy  int `xml:"SidO:Accuracy"`
  Shape     *gml.GmlShape `xml:"SidO:Shape"`
}

func NewPlanning(layer *PlanningLayer) *Planning {
  return &Planning{
    Provider: layer.Provider(),
    Accuracy: 10,
    Shape: &gml.GmlShape{},
  }
}

func (planning *Planning) XmlName() *xml.Name {
  return &planning.XMLName
}

func (planning *Planning) SetValue(key string, value interface {}) {
  switch key {
  case "Id":
    planning.Id = value.(string)
  case "LocalName":
    planning.LocalName = value.(string)
  case "ObjectId":
    planning.ObjectId = value.(string)
  case "Accuracy":
    planning.Accuracy = value.(int)
  case "Shape":
    // Shape works differently
    // planning.Shape = string(value)
  case "pos":
    // planning.AddPointshape(value.(string))
    planning.Shape = &gml.GmlShape{
      MultiSurface: &gml.GmlMultiSurface{
        Id: planning.Id + ".ln",
        SrsName: "urn:ogc:def:crs:EPSG::28992"}}

    foo := value.(string)
    // foo = strings.Split(foo, " ")[0]
    foo = strings.ReplaceAll(foo, ",", " ")
    // foo = "92336.25 441296.55 92397.62 441362.49 92457.98 441304.16 92402.69 441240.76 92336.25 441296.55"
    planning.Shape.AddSurfaceLinesToShape([]string{foo})
  default:
  }
}

func (planning *Planning) GetValue(key string) interface {} {
  switch key {
  case "Id":
    return planning.Id
  case "LocalName":
    return planning.LocalName
  case "ObjectId":
    return planning.ObjectId
  case "Accuracy":
    return planning.Accuracy
  case "pos":
    return planning.Shape
  case "Status":
    return planning.Status
  default:
    panic(fmt.Sprintf("Unknown field for GetValue: %s", key))
  }
}

//
// Internal or deprecated
//

func BuildPlanning(result *Planning, lines []string) (*Planning) {
  result.Shape = &gml.GmlShape{
    MultiCurve: &gml.GmlMultiCurve{
      Id: result.Id + ".ln",
      SrsName: "urn:ogc:def:crs:EPSG::28992"}}
  result.Shape.AddLinesToShape(lines)
  return result
}

func (result *Planning) AddPointshape(point string) {
  if result.Shape.Points == nil {
    result.Shape.Points = []*gml.GmlPoint{}
  }

  length := len(result.Shape.Points)
  Id := result.Id + ".pn." + strconv.Itoa(length)
  SrsName := "urn:ogc:def:crs:EPSG::28992"
  newPoint := &gml.GmlPoint{
    Id: Id,
    SrsName: SrsName,
    Pos: point,
  }
  result.Shape.Points = append(result.Shape.Points, newPoint)
}
