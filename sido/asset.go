package sido

import(
  "encoding/xml"
  "fmt"
  "strings"
  "strconv"

  "purplepolarbear.nl/wfs_service/models"
  "purplepolarbear.nl/wfs_service/gml"
)

//
// API
//

type Asset struct {
  XMLName   xml.Name
  Id        string `xml:"gml:id,attr"`

  SidoName  string `xml:"SidO:Name"`
  LocalName string `xml:"SidO:LocalName"`
  ObjectId  string `xml:"SidO:OBJECTID"`
  Provider  string `xml:"SidO:Provider"`
  Accuracy  int `xml:"SidO:Accuracy"`
  Shape     *gml.GmlShape `xml:"SidO:Shape"`
}

func NewAsset(layer *AssetLayer) *Asset {
  return &Asset{
    Provider: layer.Provider(),
    Accuracy: 10,
    Shape: &gml.GmlShape{},
  }
}

func (asset *Asset) XmlName() *xml.Name {
  return &asset.XMLName
}

func (asset *Asset) SetValue(key string, value interface {}) {
  switch key {
  case "Id":
    asset.Id = value.(string)
  case "SidoName":
    asset.SidoName = value.(string)
  case "LocalName":
    asset.LocalName = value.(string)
  case "ObjectId":
    asset.ObjectId = value.(string)
  case "Accuracy":
    asset.Accuracy = value.(int)
  case "Shape":
    // Shape works differently
    // asset.Shape = string(value)
  case "pos":
    // asset.AddPointshape(value.(string))

    asset.Shape = &gml.GmlShape{
      MultiCurve: &gml.GmlMultiCurve{
        Id: asset.Id + ".ln",
        SrsName: "urn:ogc:def:crs:EPSG::28992"}}
    foo := value.(string)
    // foo = strings.Split(foo, " ")[0]
    foo = strings.ReplaceAll(foo, ",", " ")
    asset.Shape.AddLinesToShape([]string{foo})
  default:
  }
}

func (asset *Asset) GetValue(key string) interface {} {
  switch key {
  case "Id":
    return asset.Id
  case "LocalName":
    return asset.LocalName
  case "ObjectId":
    return asset.ObjectId
  case "Accuracy":
    return asset.Accuracy
  case "pos":
    return asset.Shape
  default:
    panic(fmt.Sprintf("Unknown field for GetValue: %s", key))
  }
}

//
// Internal
//

func BuildAsset(result *Asset, lines []string) (*Asset) {
  result.Shape = &gml.GmlShape{
    MultiCurve: &gml.GmlMultiCurve{
      Id: result.Id + ".ln",
      SrsName: "urn:ogc:def:crs:EPSG::28992"}}
  result.Shape.AddLinesToShape(lines)
  return result
}

func (result *Asset) AddPointshape(point string) {
  if result.Shape.Points == nil {
    result.Shape.Points = []*gml.GmlPoint{}
  }

  length := len(result.Shape.Points)
  Id := result.Id + ".pn." + strconv.Itoa(length)
  SrsName := "urn:ogc:def:crs:EPSG::28992"
  newPoint := &gml.GmlPoint{
    Id: Id,
    SrsName: SrsName,
    Pos: point,
  }
  result.Shape.Points = append(result.Shape.Points, newPoint)
}
