package sido

import(
  "purplepolarbear.nl/wfs_service/core"
  "purplepolarbear.nl/wfs_service/models"
)

//
// API
//

type AssetLayer struct {
  name                      string;
  provider                  string;
  getfeatureproviderfactory models.GetFeatureProviderFactory;
  featureconvertorfactory   models.FeatureConvertorFactory;
}

func NewAssetLayer(name string, provider string, getfeatureproviderfactory models.GetFeatureProviderFactory, featureconvertorfactory models.FeatureConvertorFactory) *AssetLayer {
  return &AssetLayer{
    name: name,
    provider: provider,
    getfeatureproviderfactory: getfeatureproviderfactory,
    featureconvertorfactory: featureconvertorfactory}
}

func (layer *AssetLayer) NewItem() models.LayerItem {
  return NewAsset(layer)
}

// Implementation of Name()
func (layer *AssetLayer) Name() string {
  return layer.name;
}

// Implementation of Provider()
func (layer *AssetLayer) Provider() string {
  return layer.provider;
}

func (layer *AssetLayer) Namespace() string {
  return "SidO";
}

// Implementation of LayerType()
func (layer *AssetLayer) LayerType() string {
  return "Asset";
}

func (layer *AssetLayer) FullName() (string) {
  return layer.Provider() + " " + layer.Name()
}

func (layer *AssetLayer) FullNamespacedName() (string) {
  return layer.Namespace() + ":" + layer.Name()
}

func (layer *AssetLayer) FullTypeName() string {
  return core.Snakecase(layer.Provider() + layer.LayerType())
}

func (layer *AssetLayer) FullNamespacedTypeName() string {
  return core.Snakecase(layer.Namespace() + ":" + layer.Provider() + layer.LayerType())
}

func (layer *AssetLayer) LayerProperties() *[]*models.LayerProperty {
  return &[]*models.LayerProperty{
    &models.LayerProperty{Name: "Shape", Type: "gml:MultiCurvePropertyType", Optional: true, Nillable: true},
    &models.LayerProperty{Name: "OBJECTID", Type: "xsd:int", Optional: true},
    &models.LayerProperty{Name: "LocalName", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Provider", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Accuracy", Type: "xsd:int", Optional: true}}
}

func (layer *AssetLayer) GetFeatureProviderFactory() (models.GetFeatureProviderFactory) {
  return layer.getfeatureproviderfactory;
}

func (layer *AssetLayer) FeatureConvertorFactory() models.FeatureConvertorFactory {
  return layer.featureconvertorfactory;
}

//
// Internal
//
