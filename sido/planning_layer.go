package sido

import(
  "purplepolarbear.nl/wfs_service/core"
  "purplepolarbear.nl/wfs_service/models"
)

//
// API
//

type PlanningLayer struct {
  name                      string;
  provider                  string;
  getfeatureproviderfactory models.GetFeatureProviderFactory;
  featureconvertorfactory   models.FeatureConvertorFactory;
}

func NewPlanningLayer(name string, provider string, getfeatureproviderfactory models.GetFeatureProviderFactory, featureconvertorfactory models.FeatureConvertorFactory) *PlanningLayer {
  return &PlanningLayer{
    name: name,
    provider: provider,
    getfeatureproviderfactory: getfeatureproviderfactory,
    featureconvertorfactory: featureconvertorfactory}
}

func (layer *PlanningLayer) NewItem() models.LayerItem {
  return NewPlanning(layer)
}

// Implementation of Name()
func (layer *PlanningLayer) Name() string {
  return layer.name;
}

// Implementation of Provider()
func (layer *PlanningLayer) Provider() string {
  return layer.provider;
}

func (layer *PlanningLayer) Namespace() string {
  return "SidO";
}

// Implementation of LayerType()
func (layer *PlanningLayer) LayerType() string {
  return "Planning";
}

func (layer *PlanningLayer) FullName() (string) {
  return layer.Provider() + " " + layer.Name()
}

func (layer *PlanningLayer) FullNamespacedName() (string) {
  return layer.Namespace() + ":" + layer.Name()
}

func (layer *PlanningLayer) FullTypeName() string {
  return core.Snakecase(layer.Provider() + layer.LayerType())
}

func (layer *PlanningLayer) FullNamespacedTypeName() string {
  return core.Snakecase(layer.Namespace() + ":" + layer.Provider() + layer.LayerType())
}

func (layer *PlanningLayer) LayerProperties() *[]*models.LayerProperty {
  return &[]*models.LayerProperty{
    &models.LayerProperty{Name: "Shape", Type: "gml:MultiSurfacePropertyType", Optional: true, Nillable: true},
    &models.LayerProperty{Name: "OBJECTID", Type: "xsd:int", Optional: true},
    &models.LayerProperty{Name: "LocalName", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Provider", Type: "xsd:string", Optional: true},
    &models.LayerProperty{Name: "Accuracy", Type: "xsd:int", Optional: true}}
}

func (layer *PlanningLayer) GetFeatureProviderFactory() (models.GetFeatureProviderFactory) {
  return layer.getfeatureproviderfactory;
}

func (layer *PlanningLayer) FeatureConvertorFactory() models.FeatureConvertorFactory {
  return layer.featureconvertorfactory;
}

//
// Internal
//
