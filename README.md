# Sido Connector

Deze repository bevat de projectcode voor de SidO connector.

Als je vragen hebt, kan je contact opnemen met support, op www.purplepolarbear.nl

## Overige SidO componenten
De SidO templates kunnen gevonden worden op https://gitlab.com/fair-data-communities/sido/assets
De SidO evaluator kan gevonden worden op https://gitlab.com/fair-data-communities/sido/sido-evaluator

## Support
Voor meer informatie, kan je contact opnemen met Purple Polar Bear: www.purplepolarbear.nl

## Roadmap
De connector is onderdeel van de Sido samenwerking.

## Contributie
Zie de contributing file: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/CONTRIBUTING

## Licentie
De licentie wordt op volgende locatie beschreven: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/LICENSE
